use super::*;

#[derive(Debug)]
pub struct Ingredient(pub String);

#[derive(Debug)]
pub struct Step
{
    pub ingredients: Vec<Ingredient>,
    pub text:        String
}

#[derive(Debug)]
pub struct Modification
{
    pub text: String
}

#[derive(Debug)]
pub enum Action
{
    Step(Step),
    Modification(Modification)
}

#[derive(Debug)]
pub struct Recipe
{
    pub name:    String,
    pub actions: Vec<Action>,
    pub blurb:   String
}

macro_rules! parse_err
{
    ($tok:expr, $($fmt:expr),*) =>
    {{
        let span = $tok.as_span();

        let lines: Vec<&str> = span.lines().collect();

        for (e, l) in lines.iter().enumerate()
        {
            let mut a = 0;
            let mut b = l.len();

            if e == 0           { a = span.start_pos().line_col().1 }
            if e == lines.len() { b = span.end_pos().line_col().1   }

            eprintln!("{}", l);
            eprintln!("{}{}", " ".repeat(a), "^".repeat(b - a));
        }

        eprintln!("Parse error at {:?}", span.start_pos().line_col());
        eprintln!($($fmt),*);

        panic!("Parsing failed.");
    }}
}

pub fn render<'a>(mut toks: impl Iterator<Item=Pair<'a, Rule>>) -> String
{
    let mut rtn = String::new();
    while let Some(tok) = toks.next()
    {
        match tok.as_rule()
        {
            Rule::ctrl =>
                match tok.as_str()
                {
                    "\\("      => push_with_sp(&mut rtn, "<i>"),
                    "\\)"      => rtn.push_str("</i> "),
                    "\\SI"     =>
                    {
                        ensure_has_sp(&mut rtn);
                        write!(
                            rtn, "<i>{}</i>{} ",
                            &render(expect_group(&mut toks)),
                            &render(expect_group(&mut toks))
                        ).unwrap();
                    }
                    "\\SIrange" =>
                    {
                        ensure_has_sp(&mut rtn);
                        write!(
                            rtn, "<i>{}-{}</i>{} ",
                            &render(expect_group(&mut toks)),
                            &render(expect_group(&mut toks)),
                            &render(expect_group(&mut toks))
                        ).unwrap();
                    }
                    "\\forthe" =>
                    {
                        ensure_has_sp(&mut rtn);
                        write!(
                            rtn, "<b>For the {}</b> ",
                            &render(expect_group(&mut toks))
                        ).unwrap();
                    }
                    "\\tempC"  =>
                    {
                        ensure_has_sp(&mut rtn);
                        write!(
                            rtn, "{}&deg;C ",
                            &render(expect_group(&mut toks))
                        ).unwrap();
                    }
                    "\\&"         => rtn.push_str("&amp;"),
                    "\\comma"     => rtn.push_str(","),
                    "\\around"    => rtn.push_str("~"),
                    "\\gram"      => rtn.push_str("g"),
                    "\\metre"     => rtn.push_str("m"),
                    "\\meter"     => rtn.push_str("m"),
                    "\\litre"     => rtn.push_str("l"),
                    "\\teaspoon"  => rtn.push_str("tsp"),
                    "\\tablespoon"=> rtn.push_str("tbsp"),
                    "\\kilo"      => rtn.push_str("k"),
                    "\\centi"     => rtn.push_str("c"),
                    "\\milli"     => rtn.push_str("m"),
                    x             => {eprintln!("Unhandled ctrl {}", x);}
                }
            Rule::comma =>
                rtn.push_str(","),
            Rule::open_dquote =>
                rtn.push_str(" \""),
            Rule::close_dquote =>
                rtn.push_str("\" "),
            Rule::open_squote =>
                rtn.push_str(" \'"),
            Rule::group =>
                push_with_sp(&mut rtn, &render(tok.into_inner())),
            Rule::text =>
                push_with_sp(&mut rtn, &html_escape::encode_safe(tok.as_str())),
            _ =>
                parse_err!(tok, "Unexpected token")
        }
    }

    rtn
}

fn ensure_has_sp(s: &mut String)
{
    let needs_sp = match s.chars().last()
        {
            Some(ch) => !ch.is_whitespace() && ch != '>' && ch != '~' && ch != '"',
            None     => false
        };

    if needs_sp { s.push(' ') }
}

fn push_with_sp(s: &mut String, p: &str)
{
    ensure_has_sp(s);
    s.push_str(p);
}

fn expect_group<'a>(toks: &mut impl Iterator<Item=Pair<'a, Rule>>)
    -> Pairs<'a, Rule>
{
    let tok = toks.next()
        .expect("Unexpected end of input");

    match tok.as_rule()
    {
        Rule::group => { tok.into_inner() }
        _ => { parse_err!(tok, "Expected group") }
    }
}

fn is_end<'a>(toks: &mut Pairs<'a, Rule>, name: &str) -> bool
{
    if let Some(next) = toks.peek()
    {
        if next.as_rule() == Rule::ctrl && next.as_str() == "\\end"
        {
            toks.next();
            if &expect_group(toks).concat() != name
            {
                parse_err!(next, "Unmatched end group");
            }
            else
            {
                return true;
            }
        }
    }
    else
    {
        panic!("Reached end of tokens without \\end {}", name);
    }

    return false;
}

impl<'a> From<&mut Pairs<'a, Rule>> for Ingredient
{
    fn from(toks: &mut Pairs<'a, Rule>) -> Ingredient
    {
        let string = render(
            toks.take_while(|t| t.as_rule() != Rule::comma)
        );

        Ingredient(string)
    }
}

impl<'a> From<&mut Pairs<'a, Rule>> for Step
{
    fn from(toks: &mut Pairs<'a, Rule>) -> Step
    {
        let mut rtn = Step { ingredients: Vec::new(), text: String::new() };

        let mut ingredients = expect_group(toks);

        while ingredients.peek().is_some()
        {
            rtn.ingredients.push(Ingredient::from(&mut ingredients));
        }

        let directions = expect_group(toks);

        rtn.text = render(directions);

        rtn
    }
}

impl<'a> From<&mut Pairs<'a, Rule>> for Modification
{
    fn from(toks: &mut Pairs<'a, Rule>) -> Modification
    {
        Self { text: render(expect_group(toks)) }
    }
}

impl<'a> From<&mut Pairs<'a, Rule>> for Action
{
    fn from(toks: &mut Pairs<'a, Rule>) -> Action
    {
        let tok = toks.next()
            .expect("Unexpected end of input");

        match tok.as_rule()
        {
            Rule::ctrl if tok.as_str() == "\\step" =>
                Action::Step(Step::from(toks)),
            Rule::ctrl if tok.as_str() == "\\modification" =>
                Action::Modification(Modification::from(toks)),
            _ => parse_err!(tok, "Unexpected token in recipe")
        }
    }
}

impl<'a> From<&mut Pairs<'a, Rule>> for Recipe
{
    fn from(toks: &mut Pairs<'a, Rule>) -> Recipe
    {
        let mut rtn = Recipe {
            name:    render(expect_group(toks)),
            actions: Vec::new(),
            blurb:   String::new()
        };

        let mut blurb_toks: Vec<Pair<'a, Rule>> = Vec::new();

        while !is_end(toks, "recipe")
        {
            rtn.actions.push(Action::from(&mut *toks));
        }

        while !is_end(toks, "document")
        {
            if let Some(tok) = toks.next()
            {
                blurb_toks.push(tok);
            }
            else
            {
                break;
            }
        }

        rtn.blurb = render(blurb_toks.into_iter());

        rtn
    }
}

pub fn parse_document<'a>(toks: &mut Pairs<'a, Rule>) -> Recipe
{
    while let Some(tok) = toks.next()
    {
        if tok.as_rule() == Rule::ctrl && tok.as_str() == "\\begin"
        {
            if &expect_group(toks).concat() == "recipe"
            {
                return Recipe::from(&mut *toks);
            }
        }
    }

    panic!("Ran out of tokens before finding recipe");
}
