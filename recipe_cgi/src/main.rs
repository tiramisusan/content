use pest_derive::Parser;
use pest::{
    Parser,
    iterators::{Pair, Pairs}
};
use std::fmt::Write;

#[derive(Parser)]
#[grammar="tex.pest"]
struct TexParser;

mod recipe;
mod html;

const TEX_DIR: &'static str = "submod/";

fn main()
{
    let path = std::env::var("PATH_TRANSLATED")
        .expect("Could not get PATH_TRANSLATED");

    if path.len() == 0
    {
        do_list();
    }
    else
    {
        do_recipe(&path);
    }
}

fn get_recipe_name(path: &std::path::Path) -> Option<&str>
{
    let ext = path.extension()?.to_str()?;

    if ext == "tex"
    {
        let name = path.file_stem()?.to_str()?;

        if name == "book" || name == "links"
        {
            None
        }
        else
        {
            Some(name)
        }
    }
    else
    {
        None
    }
}

fn do_list()
{
    let mut recipes: Vec<String> = Vec::new();

    let read_dir = std::fs::read_dir(TEX_DIR)
        .expect("Could not read TeX directory");

    for result in read_dir
    {
        let entry = result.expect("Could not read TeX directory entry");

        if let Some(name) = get_recipe_name(&entry.path())
        {
            recipes.push(String::from(name));
        }
    }

    recipes.sort();

    html::render_list(&recipes);
}

fn do_recipe(path: &str)
{
    if !path.chars().all(|ch| ch.is_alphanumeric() || ch == '-')
    {
        panic!("Invalid path");
    }

    let fname = format!("{dir}/{path}.tex", dir=TEX_DIR, path=path);
    let tex   = std::fs::read_to_string(fname)
        .expect("Could not read TeX file");
    let toks  = TexParser::parse(Rule::file, &tex)
        .expect("Error parsing TeX file")
        .next()
        .expect("TeX file had no tokens");

    let recipe = recipe::parse_document(&mut toks.into_inner());

    html::render_recipe(recipe, path);
}

