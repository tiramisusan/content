# Pastebin

I've built a simple pastebin for sharing files.

## To upload a file from your browser

Visit the [file upload form](/paste/form).

## To upload a file over HTTP

To upload a file, either send it as an HTTP `POST` or `PUT` request to
`https://tiramisusan.uk/paste`. For example with curl

```
 $ echo Hello World > test.txt
 $ curl --upload-file test.txt https://tiramisusan.uk/paste
https://tiramisusan.uk/paste/9
 $ curl https://tiramisusan.uk/paste/9
Hello World
```

## To upload a file over Telegram

To upload a file, send a message to
[@tiramipaste\_bot](https://t.me/tiramipaste_bot).

$$photo(/img/paste/telegram.jpg)

