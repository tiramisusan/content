# A Walk along the South-West Coast Path

In October 2022, we walked from the Isle of Purbeck to Weymouth over a couple of
days, stopping to camp each night.

## Day 1 - From Purbeck to Kimmeridge

On the first day, we took the bus from Poole to the village of Kingston, and
walked to Kimmeridge Bay following the coast.

$$photo(/img/coast-path-walk/st-james-church.jpg) St. James Church in Kingston

$$photo(/img/coast-path-walk/houns-tout.jpg) A view of Houns Tout from the west

There were plenty of blackberries in the bushes to eat along this part of the
journey, and since the brambles had been growing so much, the paths were far
narrower than they would be in other seasons. Every time we had to stop and wait
for another person to pass, we would eat blackberries.

$$photo(/img/coast-path-walk/banded-cliffs.jpg) Banded cliffs

$$photo(/img/coast-path-walk/kimmeridge-bay.jpg) Kimmeridge Bay

Along the cliff-tops there were Sea-Cabbage plants, which we picked and kept in
our bags for later.

$$photo(/img/coast-path-walk/st-nicholas-church.jpg) St. Nicholas Church in the
Village of Kimmeridge

$$photo(/img/coast-path-walk/fire-umberella.jpg) A fire in the rain

Once we arrived at Kimmeridge, we explored the village, and had some food.
We made a fire in the dark when we returned -
it was raining, so we kept our tinder dry with an umberella until it had caught.
We cooked the sea-cabbage we had collected earlier with miso paste to make a
soup, kept some of it in a thermos for breakfast, and had tea before bedtime.

## Day 2 - From Kimmeridge to West Lulworth

On the second day, we walked from the village of Kimmeridge to the popular
village of West Lulworth. As the weather was still unsettled, we decided to walk
inland along the hills. Like much of the land in Dorset, the land to our north
was owned by the millitary, and is used as a firing range.

$$photo(/img/coast-path-walk/blackthorn.jpg) An old windswept hill-top
blackthorn

$$photo(/img/coast-path-walk/sheep-in-the-road.jpg) Sheep in the Road

$$photo(/img/coast-path-walk/cow-range.jpg) Moo-ving target practice

We climbed around Worbarrow bay, passing [Flower's
Barrow](https://en.wikipedia.org/wiki/Flower%27s_Barrow), an ancient hill fort
that has half fallen into the sea, as the cliff eroded below it.

$$photo(/img/coast-path-walk/worbarrow-tout.jpg) Worbarrow Tout

$$photo(/img/coast-path-walk/worbarrow-bay.jpg) Worbarrow Bay

The next bay is Lulworth Cove, with the famous Durdle Door to the West. We
camped in Lulworth for the night, and bought some food from the village.

$$photo(/img/coast-path-walk/durdle-door.jpg) Durdle Door

$$photo(/img/coast-path-walk/caves.jpg) Exploring the caves next to Durdle Door

$$photo(/img/coast-path-walk/victory-sandwich.jpg) A victory sandwich after a
long day

## Day 3 - From West Lulworth to Weymouth

On the final day, we walked from Lulworth to the town of Weymouth, where we
could catch the train home. We didn't take many pictures, as we were tired, but
we found lots of sloes to eat along the way, and we passed through Osmington
Mills, a village with a beautiful little pub, which we need to return to for a
meal or a night at some time.

$$photo(/img/coast-path-walk/obelisk.jpg) There is a mysterious obelisk and a
nice view to the West of Lulworth

$$photo(/img/coast-path-walk/burning-cliff.jpg) A view of Weymouth from the
cliffs to the East

$$photo(/img/coast-path-walk/ringstead-bay.jpg) A view of the burning cliffs
from Ringstead Bay.

$$metadata
 * `title` `A Walk along the South-West Coast Path`
 * `date` `2023-03-26`
 * `authors`
   1. `Francis Wharf`
   2. `Greta De Paoli`
