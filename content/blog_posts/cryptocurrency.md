# I don't like cryptocurrency...

I don't really know anything about either economics, or the technical details of
cryptocurrencies. I do know that I dislike them though, and I will now make
various unfounded assertions.

Distributed block-chains ledgers (on which cryptocurrencies like Bitcoin are
built) are perfectly fine as a technology - few technologies are problematic in
and of themselves. It can be all too easy to mismatch solutions and problems
however, especially if a solution becomes fashionable, or if either the problem
or solution are nebulously defined.

## Why do block-chains make sense for currency?

Block-chains maintain a public sequence of blocks of information, where the
opportunity to append information to the chain is fairly democratised to each
participating unit of computing power. Each participant in the block-chain can
independently verify the chain's integrity. The key feature of the block-chain
is that its properties do not depend on trust. There is no way for any party,
malicious or otherwise, to take control of a portion of the block-chain greater
than that party's proportion of computing resources.

These properties are achieved by means of cryptographic hash functions -
functions that produce seemingly random outputs for given inputs, and that while
easy to perform, are very time-consuming to invert. Finding inputs to a hash
function that produce the number zero is the mechanism by which computing effort
is proven in a block-chain. Each block in a block-chain is accompanied by a
number which, when put into a hash function, along with a reference to the
previous block, and the information stored in the block, results in the number
zero. This means that for each block in the chain, every participant can verify:

 1. The rough amount of computing effort expended in creating the block. As the
    hash function is difficult to reverse, it takes a known amount of effort to
    find an input for which its output is zero.

 2. That this block was produced in sequence, after the previous block in the
    chain. A reference to the previous block is fed into the hash function, so
    it would only have been possible to find a solution to the hash function
    after that previous block had been published.

 3. That the information in this block was produced by the same party that
    expended the computing effort to solve the hash function. Since the
    information itself is also fed into the hash function, anyone wanting to
    append different information onto the block-chain would need to solve a
    different hash problem.

If this system sounds inefficient, that's a feature! Efficiency is traded away
for security, trust, and ability-to-be-distributed. When a block-chain is used
for cryptocurrency, it is the inefficient solving of hash functions that
provides value to what would otherwise be a simple sequence of blocks. Computing
power costs money, and so the privilege of being the one to append to the
block-chain has a calculable price, as long as someone is willing to pay it. The
difficulty of solving hashes gives cryptocurrencies their value, just as the
difficulty of mining for gold gives that value too. This is presumably where the
term cryptocurrency mining comes from.

## I don't think cryptocurrencies are a secure medium of exchange

In order for money to work, a more important property than scarcity or provable
ownership is its ability to be traded for other things. When money is exchanged
for goods, [there is always the risk that the goods never
materialise](https://en.wikipedia.org/wiki/Complaint_tablet_to_Ea-n%C4%81%E1%B9%A3ir).

When using physical currency, the goods can often be seen while exchanging
money, and any attempt on the part of a seller to take the money and run comes
with the risk that the buyer is simply faster than them. In the age of remote
commerce - firstly through mail, and secondly over the internet - the
traditional financial system and law enforcement have developed practical
measures to allow money to be recouped, and scammers to be traced and brought to
justice.

Despite its focus on trust and security, cryptocurrency is not designed to be
securely exchanged for something else. You can, extremely securely, transfer
your cryptocurrency to someone else, but that doesn't help whatsoever if they
run away with the money. Because trust is rooted in the block-chain itself,
there is no easy way to recoup or trace stolen funds. Cryptocurrencies expect to
be used in digital commerce, but have the same security features as physical
currency.

Solving this would require somehow integrating the exchanged goods themselves
into the block-chain. The closest anyone has come to achieving this is - with
great excitement - putting URLs of weird JPEGs into a block-chain. This was not,
as it turns out, particularly useful or revolutionary.

If the appeal of cryptocurrencies is that they provide an alternative financial
system whose trust is rooted in the cold mathematics of cryptography, then it is
hard to see how they can maintain this benefit while being used for anything
that hasn't also been engulfed by the same cumbersome and inefficient system.

## An efficient currency isn't a get rich quick scheme

Cryptocurrencies are often promoted as massively profitable investments. It is
hard to square this with them being usable currencies. A currency that massively
increases in value over time is not the kind of thing that a healthy economy is
built on.

If cryptocurrencies are to gain widespread adoption, then at some point, they
will need to stabilise in price, and the costs of whatever profits have been
generated by their massive increases in value will have to be settled with
someone.

While fiat currencies are backed by governments and banks who have a strong
interest in their continued stability, cryptocurrencies are promoted as not
requiring trust in any single organisation. They do however, like any currency,
require their users to continue believing that they're worth something.

If a cryptocurrency actually managed to be a usable currency, then the idea that
had been sold to a large proportion of its holders would fail. It would no
longer be a Ponzi scheme, because functional currencies don't act like Ponzi
schemes. It would then be in the interest of those same individuals to simply
start over, with a new currency.

For now though, the inefficiencies of cryptocurrencies seem more than excusable
for money laundering and scamming, but I don't see much legitimate use for them
beyond this.

There might be some more interesting uses to the underlying technology though.
Supply chain logistics feels like a use-case where mutually untrusting parties
might derive enough benefit from a secure shared ledger of information to
justify the costs of using it.

$$metadata
 * `title` `I Don't Like Cryptocurrency`
 * `date` `2024-10-17`
 * `authors`
   1. `Francis Wharf`
