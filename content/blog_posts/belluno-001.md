# Belluno?

[Belluno](https://en.wikipedia.org/wiki/Belluno) is a city, supposedly in the
province of itself.

From the point of view of an environment, the only way for life to survive is
for it to evolve into a shape dictated to it. From the point of view of life
however, there is no death. Death is a nothing; impossible to experience. The
tables are turned, and the only way the environment can survive is for it to
evolve into a shape dictated to it by life.

## Chapter 1. Good morning Belluno

The night was getting cold. Some crumbs of muddied snow brought in by mountain
boots were climbing the staircase to the city - each stair an escalator. They
passed the photo-booths, vending machines and piles of fizzing mangled radio
equipment indifferently.

"It's time to leave," the guard at the top of the staircase was equally
indifferent. "The parking regulations won't allow us to stay here much longer."
He patted a copy of a legal code in the public library booth - written in the
court of Charlemagne, to be read by the illiterate - and flipped a switch
behind the binbag vending machine. The snow finally overcame its apathy and
vanished into thin air with surprise. The guard was still indifferent, but
disappeared nonetheless.

The sun rose, and the city of Belluno inhaled the fog from the plain below,
twisting around the roses and brambles rooting the city to the ground, and
painting them with silvery frost. With its next breath, three busses-worth of
school children were sucked into the city, through its intricate snout of
escalators. Some of the children searched for change and scrabbled for snacks
in the vending machines. Some dove into the photo booths with their friends, or
themselves. Some morosely rummaged through the now-mostly-empty library booth.
And so the city could tell what kind children were rising into its lungs as
they slotted into the scent receptors that lined the tube of escalators.

Gregorio Visentin was a patriotic Veronan schoolboy. His father worked as a
Fascist at the local Biscuit Company (inc. (except for tax purposes)), and his
mother stole Louis Vuitton logos from the handbags of passers by, and sewed
them onto binbags to sell at the market. He'd never been to Belluno before,
though he'd heard rumors about it - "bleedin' massive clock; what're they
like". As he left the top of the escalator, he noticed that it had accidentally
been built too high and he needed to climb back down a few more steps to leave
the terminus.

He spilled into the piazza with his friends. To them the city was sleepy and
thoroughly dull. Their teachers herded them towards the theatre, where they
were to watch a performance of Romeo and Juliet. The actors had chosen to
interpret the play as a type of pantomime - "Wherefore art thou Romeo?" A
gesture to the audience. Stern teenaged silence. Gregorio ruffled his backpack,
held between his legs. It felt as if the fog outside had entered the theatre,
and each breath was slow and enumerated. He was sitting at the end of an isle,
and absently stared at the side door of the theatre, visualising escaping the
room over and over again. The sounds of the play were an indistinct muddle,
except when something sudden brought Gregorio back to reality. A round of jeers
as an actor forgets his cue; he wondered how long this could go on for. A crash
as an oversized two dimensional plywood bottle of poison is dropped by an
anxious friar; he noticed that the two teachers seemed to have left their seats
at the back of the theatre, maybe he could escape after all. A premature round
of applause as the first of the characters finally began to die; he decided to
put his backpack on, get up and sneak to the door. His legs felt cold and
aching as the blood began to flow through them again. He opened the door as
gently as he could and slipped through it, blinking in the humming tube
lighting of the corridor.

He straightened himself out and wandered down the corridor, examining at the
posters advertising plays - some new, and some peeling off from their frames to
expose their predecessors. It looked as through the fog from outside had
started to seep in through the cracks under the doors and around a ceiling
panel that had been left ajar. Left his backpack on the floor, and pulled
himself on top of a vending machine selling yesterday's lottery tickets to move
one of the tiles aside. The space above the ceiling was a more open world than
the one below. Gregorio could see by the glow leaking from the back of the
light fixtures, casting uninterpretable shadows of workmen's tools, stray wires
and forgotten planks of wood onto the rafters. The source of the fog was
clearer now. It was rising from a square group of tiles ahead of him. Curious,
he crawled towards them. As he reached the panels, he began to notice that the
fog didn't seem quite right.  Rather than the unwelcome but friendly wet dog of
Veronan fog that he so loved, the fog was light and thin and bitter. He was
pondering the meaning of this when one of the tiles below his knees gave way.
He put his weight onto his hands, but the tiles there failed too. He was in
midair, halfway down a brightly lit bathroom, full of choking smoke. His ankle
hit the side of a porcelain sink. He didn't have time to cry out - the only
sound he experienced was of his skull smashing onto a polished white concrete
floor, not that he would remember it.

$$metadata
 * `title` `The Belluno Story - Chapter 1`
 * `date` `2022-12-07`
 * `authors`
    1. `Francis Wharf`
