# Chapter 2. Wake up Belluno

"Well I wasn't going to smoke outside was I? I could get a cold."
$br The two teachers were arguing with a firefighter outside the burning
skeleton of the theatre.
$br "And who puts plastic bins that can burn in bathrooms anyway?
Absolute cheapskates. That's who I'd like you to talk with."
$br The firefighter didn't really mind. It was nice to finally have something to
do. The most exciting thing to happen in the last two months was an aborted
rescue of a cardboard cut-out of the Queen of England that someone had
mistakenly reported as being trapped on a balcony.

The remainder of the children dispersed through the Piazza as one of the
firefighters emerged from the Theatre holding Gregorio's limp and blistered body
at arm's length. One of the teachers tried to grab the lighter from the other as
she was striking up another cigarette.

Gregorio woke up on a slightly warped wooden floor, worn soft and smooth like
marble towards the centre of the room. The wallpaper was a faded burgundy, and
was raised into perpendicular ridges tracing the forms of windows or doors that
might once have existed. Gregorio pushed himself unsteadily to his feet. He
noticed that he smelled like a Bavarian smoked cheese, and that his head still
ached. It felt like someone had stretched the skin of his scalp taut over a
stress-ball, and someone was still using it. On the floor, there was a desk-lamp
shining directly into a wooden crate of bread and cakes. There was no other
light-source, only a doorway, which seemed more a source of dark.
Gregorio wandered out onto an unlit landing, and followed a staircase down
into an open room. As his foot left the bottommost stair, the lights turned on,
and a tall woman standing directly in front of him firmly struck a bell on a
desk. He immediately retreated around the corner of the staircase, and back up
into the darkness.

After breathing for a moment, Gregorio explored the top of the balcony and the
little room where he had woken up more thoroughly, making sure that there really
was no other way to leave. Once that had been done as many times as Giorgio
could justify to himself, he made his way back down the staircase, a little
afraid, but mostly sheepishly. Again, as he stepped down from the final stair,
the lights flicked on, and the woman struck her bell. "Hello, I-", Giorgio tried
to say, but she had promptly left the room. A few moments later, she strode back
into the room, but this time stood behind the desk.
$br "You rang?"
$br "What is this?"
$br "I see, do you have a table reservation?"
$br "What?"
$br "Just one moment, and I will check whether we can fit you in."
$br She stared blankly into space for a moment.
$br "We should be able to squeeze you in Sir. Please, follow me."
$br Gregorio watched as she entered a pair of wooden doors beside her little
desk. After pausing for a moment to try and understand what was happening, and
concluding that there wasn't much point, he followed her into a thoroughly
abandoned café. She pointedly pulled out a chair besides a table concealed in a
little corner as far away from the windows as possible, and sat him down.

A small coffee quickly appeared in a little tartan cup. It was excellent, and
Gregorio drank it while he considered what to do. He had to go and find the rest
of his school, or he would be struck alone in an unfamiliar city. As he finished
his coffee, he was immediately handed another. He frowned at the little cup,
this time neon pink with the words "Coffee Break" in aggressively cursive
letters. "I need to be going." He realised that he had been involuntarily
holding his breath in the presence of the presumed café owner, and his voice
sounded like someone had stepped on a bagpipe. She remained stood by the table
as he opened the door and ploughed a furrow through the fog outside.

There seemed to be in a market square outside the café, though only a single
edge was visible. Stalls selling all manner of goods emerged one at a time from
the fog in every direction, accompanied by enthusiastic cries of special offers,
and the chatter of negotiation and arguments. As Gregorio approached each stall
however, the proprietor was invariably alone and silent - either forcefully
avoiding eye contact, or looking un-approving and gently shaking their head.
Some of the stalls were recognisable, selling bread, vegetables or clothes,
others were piled high with the strangest items. One was selling surfboards and
deck chairs, while another was selling muskets and cannons, and third selling
posters exclusively from the Soviet Union. Gregorio preferred to be surrounded
by the familiar Veneto fog than to try his chances with these strange people. He
knew that he had to find his way back to his friends and to home. Eventually,
one of the other sides of the square emerged into view, lined with off-white
pillars like the mouth of a whale.

Gregorio followed the perimeter of the square to a corner, and continued down a
little cobbled street leading away. The fog made it difficult for him to orient
himself in time or direction. The city did not sound as though it had done in
the morning. It was now noticeably and electrically quiet. He could hear every
one of his footsteps as distinct and different sounds as the smoothness and age
of the stones changed. Vague light seeped through from every direction, unable
to cast a shadow. The street opened out into another small square, and on the
other side was the theatre where he had spent the morning. He ran towards it,
but as he got closer he noticed that it was the wrong shape. The roof was
blackened and half caved in. He saw someone get up from a bench at the side of
the little square, and the owner of the coffee shop was in front of him.
$br "Your friends have left without you I'm afraid."
$br Gregorio felt as if he had just touched the ground at the bottom of a
long plastic slide and discharged an electric shock from his body.
One by one he felt his organs weigh in on the news. He turned to hide his face,
but was immediately confronted by every merchant from the market, waving their
various wares. He stepped back from the salami and sticks of bread thrust at
him, as the merchants began to discordantly chant their prices and offers. He
was driven backwards until he was pressed up against a wall, where he began to
cry. He closed his eyes, let his legs collapse beneath him, and he sobbed and
screamed for as long as he could. After a while, he caught control of his
breathing. He couldn't hear anyone anymore.

He opened his eyes and saw a woman sitting on the edge of a fountain. He
wasn't sure whether the fountain had been there before, but it felt as though it
would have been an oversight by the town council were it not.

$$metadata
 * `title` `The Belluno Story - Chapter 2`
 * `date` `2023-03-07`
 * `wip` `false`
 * `authors`
    1. `Francis Wharf`
