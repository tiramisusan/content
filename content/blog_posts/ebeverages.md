# E-Beverages



## Teapot

## Moka

```
#!/bin/bash
# A simple script to brew coffee using a Moka over HTCPCP

# URL Endpoint
URL=http://tiramisusan.uk/moka

# GET any residual coffee
curl $URL

# BREW a new Moka
curl -X BREW -d "start" --header "Content-Type: message/coffeepot" $URL
sleep 1

# Get the new moka until coffee is (201 Create)'d
while [[ $(curl -s -o /dev/stderr -w "%{http_code}" $URL) -ne 201 ]]
do
    sleep 1
done
```
