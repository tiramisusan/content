# A Winter Walk from Cortina

This week, we found ourselves in
[Cortina d'Ampezzo](https://maps.app.goo.gl/B9nueLcxfbuU212j8) in the Dolomites,
and decided to follow a
[route we found online](https://www.montagnadiviaggi.it/2012/06/escursione-cortina-dampezzo-il-lago.html)
for a winter walk.

The route is easy and relaxing, and mostly follows roads, but the paths through
the woods can be harder to trace if a lot of snow has recently fallen. Wet
ankles await the foolhardy.

## Leaving town

We started in the town or Cortina d'Ampezzo, at around 1200m.

$$photo(/img/winter-walk/view-from-cortina.jpg) Cortina d'Ampezzo.

We walked to the north side of the town, being careful to avoid the main roads,
as the cars had crushed the snow to ice, and many of the roads had no separate
pavements. We took the lane to the village of
[Cadin di Sopra](https://maps.app.goo.gl/wsvq92VSDsiqSuAM9), overlooking
Cortina, with views of the surrounding mountains.

## The Climb to Lake Ghedina

After leaving town, a path led up the hills to the West. We were the first
people to walk it that day, and yesterday's footprints had been softened by the
overnight snow.

$$photo(/img/winter-walk/path.jpg) Into the hills.

The path ascends through the woods before joining a road leading to a little
lake with, the [Ristorante Saliola](https://saliolalagoghedina.com/) on one of
its banks. The lake was full of both fish and ducks.

$$photo(/img/winter-walk/lake-ghedina.jpg) Lake Ghedina.

We arrived to find a snowman without a face, so pebbles collected from the lake
became a nose and eyes. The ducks misinterpreted this as an attempt to feed
them.

$$photo(/img/winter-walk/ducks-hungry.jpg) The ducks are hungry.
$$photo(/img/winter-walk/ducks-angry.jpg) The ducks are angry.

## The descent to the river

After visiting the lake, we returned to the woods, and ascended further to the
highest point in the walk, at 1460m. Here, the path forked, and we began to
descend to the North, rather than continuing to climb the mountain. The snow was
thicker here, and the weather colder. There were more views of the surrounding
mountains from this side of the hills, as the River Boite cuts itself a steep
rocky valley.

$$photo(/img/winter-walk/descent-view.jpg) A mountain friend.

The descending path meets the river next to
[a campsite](https://campingolympiacortina.it/), where is it crossed by a
bridge.
There was plenty of snow to throw into the river below.

$$photo(/img/winter-walk/bridge.jpg) Snow to throw and a place to throw it from.
$$photo(/img/winter-walk/bridge-view.jpg) A home in search of a salmon.

## Following the river back to the town

We followed the River Boite downstream, South towards Cortina. As we followed
the river, our tracks were joined by those of a family of deer, who we had
previously seen while descending. At one point we entered a shallow meander in
the river alongside the deer tracks - them to drink, and us to find shiny
stones.

$$photo(/img/winter-walk/river.jpg) Lots of nice rocks.
$$photo(/img/winter-walk/river-2.jpg) The River Boite.

From here, the path shadows to the river, until it ascends through the woods and
emerges onto a  blinding snowy expanse.

$$photo(/img/winter-walk/return-forest.jpg) The path through the valley of the Boite.

We took our lunch on the open slope - some focaccie and pastries from [one of
the bakeries in Cortina](https://maps.app.goo.gl/M94w6yvktwXANRBT6).

$$photo(/img/winter-walk/slope-view.jpg) A last view of the mountains.

The path then led back to Cadin di Sopra, where we rejoined our footsteps, and
enjoyed an easier walk back to town, as the sun had begun to melt the ice.

$$photo(/img/winter-walk/skier-sign.jpg) Danger - Skier (single,) (large)

$$metadata
 * `title` `A Winter Walk from Cortina`
 * `date` `2022-12-06`
 * `authors`
   1. `Francis Wharf`
   2. `Greta De Paoli`
