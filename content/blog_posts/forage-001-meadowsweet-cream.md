# Meadowsweet and Skin cream

This summer, I found a field absolutely full of meadowsweet, and decided to use
it to make some skin cream. Meadowsweet smells beautiful, and contains plenty of
salicylic acid, which should help reduce skin redness, inflamation and
irritation.

I gathered a basket-full of meadowsweet flowers and leaves. Meadowsweet likes to
grow near rivers, so gathering it required some balance to avoid falling into
the mud. It is also named "*Queen of the Meadows*", and aptly so! The flowers
grow in beautiful clusters, and smell a little like elderflower, almonds and
bubblegum. The seeds grow as tiny green spirals, and the leaves grow in groups
of jagged pairs, each tipped with a trident.

Yarrow meanwhile is a common plant in any field or roadside, so I found some in
a park. It is also named "Soldier's woundwort", and the flowers and leaves are
traditionally used to treat wounds and stop bleeding. The leaves are silvery,
and smell like a well-seasoned fried cabbage. I've never used it in cooking, but
apparently it can be used as a herb.

$$photo(/img/meadowsweet-cream/basket.jpg)
    A full basket of meadowsweet and yarrow. I always like to use teatowels to
    separate different types of plants.

To make my cream, I used a mix of beeswax and olive oil, though plenty of
alternatives will work just fine. I started by melting the oil and wax together
in a bain marie. To check the ratio of wax to oil, I dipped my
finger into the cream, let it cool, and checked how the final product would
feel.

$$photo(/img/meadowsweet-cream/wax.jpg)

I then crushed up my plants in a mortar and pestle, and added them to the bain
marie. I put on a lid, and left them to soak in the hot wax for a few hours.
The yarrow has a garlicky smell when cooked like this, and the meadowsweet
really starts to smell like medicine.

$$photo(/img/meadowsweet-cream/meadowsweet.jpg)
    Meadowsweet flowers ready to be crushed.

$$photo(/img/meadowsweet-cream/yarrow-1.jpg)
    Yarrow ready to be crushed.

$$photo(/img/meadowsweet-cream/yarrow-2.jpg)
    **Smoosh!**

After cooking, I used a seive to strain the liquid from the plants. I'd expected
to have to use a cheesecloth, but the flowers and leaves strained themselves
remarkably well. I simply piled everything into a seive, and then put the seive
inside the still-hot bain marie and replaced the lid, to keep the cream liquid,
so that it could drip down and out of the seive.

I then simply poured the cream into pots and let it harden. If you want a nice
flat surface to your cream, you need to pour it slowly and carefully, and then
not move the pot whatsoever until it dries. Thus far, it seems to be effective
in making insect and plant stings better, as well as dermatitis, though I've not
tested it scientifically whatsoever.

$$metadata
  * `title` - `Making Meadowsweet Skin Cream`
  * `date` - `2023-09-05`
  * `authors`
    1. `Francis Wharf`
