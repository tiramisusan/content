# Moka Man

In a market in Padova, Greta and I came across a stall by [Think
Up](https://www.thinkup-handmade.it/), selling small lamps and decorations made
from reused objects. We couldn't buy anything at the time, but Greta needed
to replace her Moka, and so we decided to turn our old one into a little
sculpture.

## Photos

$$photo(/img/moka-man/stirring.jpg) It's him, a small friend

We built him around Christmas-time, while visiting my parents, using pieces of
junk from around their house. At some point, we will add some more peices to him
and install a lamp in his head. I'll put more photos here once that happens, but
he's still fun for the time-being.

$$photo(/img/moka-man/left.jpg) To the left
$$photo(/img/moka-man/right.jpg) To the right

His head is built from the upturned top half of the Moka, and his body is built
from the boiler part of the Moka. They're attached together with some metal rope
fasteners, which keep the head tilted upwards in a little pose.

$$photo(/img/moka-man/open.jpg) Inside his head

His hair is made from resistors, soldered to a ring of copper in the thread on
top of his head, and his eyes are potentiometers with a few extra electronic
components soldered to them for decoration.

$$photo(/img/moka-man/close.jpg) Are you ready for your close-up?

His hands are self-tapping screw hooks that are tapped directly into the
aluminium boiler body. One is driven through the drilled-out safety valve.

His fingers are British [BS 1363](https://www.plugsocketmuseum.nl/British1.html)
plug socket pins, held in place on the hooks by tightening their brass screws.
In one hand, he holds a silver spoon, attached by soldering a fuse-holder to it.

$$photo(/img/moka-man/hand.jpg) He holds the standard of his people

One of his feet is the handle that used to go on top of his lid, and the other
is the base of a candle stick.

$$photo(/img/moka-man/feet.jpg) Foot pic
