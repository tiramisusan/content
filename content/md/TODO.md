# TODO (For this website's technology)

This is my list of things that need to be done for this website!

Whenever I need something to do, I will look at this list, and tick something
off.

## Servoidardi Core (Webserver)

### Code structure

 [ ] Split services into separate repositories.
 [x] Add preprocessor chains.
   [ ] Add directory listings as preprocessors.
   [ ] Add caching as preprocessors.
 [ ] Add postprocessor chains.
   [ ] Generalize the majority of file services as postprocessors.

### Command-line options and config

 [ ] Add `--help` commandline option.
 [ ] Add `--serve-dir` commandline option to serve files from a directory with
   no config.
 [ ] Add `--override-port` commandline option to override the specified TCP
   port.
 [ ] Add `--write-port` commandline option to use an auto-assigned port and
   write it to a file.

### HTTP

 [ ] Use compression encoding for responses.
 [x] Handle `PUT` and `POST` requests.
 [ ] Handle persistent connections.
 [ ] Handle duplicate headers.
   [ ] In requests.
   [ ] In responses.
 [x] Handle non-200 success responses.
   [ ] Handle `Range` and HTTP 206.
 [ ] When writing content, avoid excessive allocations by using `io::copy_buf`
     instead of `io::copy` when possible.

### Performance

 [ ] Add benchmark tests.

### Management and debug tools

 [ ] Add debug logging service to remotely query logs, or trigger logs of
     particular requests.
 [ ] Add debug config service to query the current config.
 [ ] Add debug status service to query the status of currently running services.
 [ ] Add debug statistics service.
 [ ] Add HTML UI for presenting debug information.
 [ ] Add size limits for service `Scratch` pools, and configurable policies.

## Servoiardi Services (Webservices)

### Blog Service

 [ ] Add keyword system.

### Files Services

 [x] Add timestamps in directory listings.
 [x] Generalize index files.

#### Matthewdown Files Service

 [ ] Add `?source=1` option to view matthewdown source.

#### CSS Files Service

 [ ] Add CSS files service.
   [ ] Preprocess include directives.
   [ ] Preprocess variables.

### Pastebin

 [x] Add a pastebin service.
 [x] Add upload from Telegram.
 [x] Add upload from console.
 [ ] Add upload from browser.

## Matthewdown (Markup language)

### Language Features

 [x] Add `[ ]` checkboxes (**So I can use them in this list!**)
 [ ] Add markdown-style photos.
 [ ] Add citations and references.
 [ ] Add code syntax highlighting.

### Library Features

 [x] Deserialize command arguments directly into types like `i32`, using a
     common type with Metadata.

### Performance

 [x] Add benchmarks to keep track of performance.
 [ ] Rework HTML visitor to stop making copies of the string `"p"` on the stack.

