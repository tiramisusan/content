# Elderberry

Elder is a small tree that grows in sunny hedgerows and nearby rivers. The
leaves have serrated edges, and often take on a yellow colour that makes it easy
to spot the tree from a distance.

$$photo(/img/foraging/elderberry/tree.jpg)
    Elder tree in late summer.

In the late summer, the trees grow bunches of berries, that turn from green, to
red, to deep purple-black as they ripen. Elder trees are enthusiastic producers
of fruit, and can easily burden themselves too heavily, dying back and
abandoning branches to die during the growing season. Once ripe, there is often
more fruit than birds can eat, and it can spoil on the tree, succumbing to
mould, insects, or simply drying and shrivelling.

When ripe, an easy way to gather the berries is to hold a basket under the tree,
and rub the bunches of berries. The unripe berries will generally stay attached,
and the ripe ones will fall into the basket.

$$photo(/img/foraging/elderberry/greta.jpg)
    Picking elderberries.

$$photo(/img/foraging/elderberry/basket.jpg)
    A basket of elderberries.

Once gathered, pour the berries into a bowl of water, and wash thoroughly.
Insects enjoy elderberries, and often come along for the ride, so watch out for
any, particularly snails. The unripe berries and stems should float to the top,
and can be poured off, while the ripe and over-ripe elderberries will sink to
the bottom. If the elderberries were gathered late in the season, the over-ripe
elderberries will need to be manually removed.

$$photo(/img/foraging/elderberry/bowl.jpg)
    Elderberries cleaned and ready for cooking.

Elderberries need to be cooked before use, and can be poisonous in large
quantities when raw. When cooked, they can be used for making jams or wine.

$$photo(/img/foraging/elderberry/frozen.jpg)
    Frozen elderberries ready to use.

$$photo(/img/foraging/elderberry/wine.jpg)
    Straining the fruit pulp from an elderberry wine.
