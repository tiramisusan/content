# Sloes

Sloes are the fruit of the
[blackthorn](https://www.wildlifetrusts.org/wildlife-explorer/trees-and-shrubs/blackthorn).
They are similar to their close relatives
[bullace](https://en.wikipedia.org/wiki/Bullace) and
[damson](https://en.wikipedia.org/wiki/Damson), though I've never worked out
where a large sloe ends, and a bullace begins. No one is likely to complain
about the two being mixed up though.

## Habitat & Identification

Blackthorn is a large shrub, common in hedgerows and at transitions between
woodland and grassland. It is therefore very common to find by the sides of
paths, particularly surrounding the older field borders and settlements.
It can grow in a wide variety of conditions, but seems to thrive in exposed
areas near to water, but that don't become boggy. Shallow valleys and nearby old
ditches seem to be favourites.

Blackthorn has long thorns, though some are larger or smaller and some plants
have fewer or more. Presumably selectively breeding blackthorn to act in its
traditional role as a way of containing livestock has led to some varieties
being significantly thornier.

$$photo(/img/foraging/sloes/windswept-tree.jpg)
    A windswept blackthorn tree on top of a hill near the sea in Dorset.

In the spring, blackthorn bushes paint hedgerows in brilliant white as winter
ends. They are one of the first plants to flower, and do so before their leaves
have grown. This is one of the easiest times of year to spot blackthorn, as long
as you remember where they were by the time autumn arrives.

Their leaves are dark green, and shaped like slim ovals with serrated edges.
From a distance, they are most readily identifiable by the crooked shapes of
their branches, though several shrubs look indistinguishable, especially in the
winter when they have no leaves.

$$photo(/img/foraging/sloes/winter-tree.jpg)
    A blackthorn tree November, having lost its leaves.

Luckily, sloes are very easy to identify. They are roughly 15mm spheres that are
either matte blue-grey, or glossy navy-blue, becoming darker if they are wet,
and as they ripen. The amount produced by any one tree can vary wildly, from
only a dozen or so per tree, to thousands. Sometimes the number varies by tree,
but sometimes the conditions are right in a particular area, and a large group
of trees all produce an abundance.

## Picking and Eating

Traditionally, sloes are harvested after the first frost of the year, though in
Southern England, the season is usually ending by this time. The longer they
are left on the tree however, the sweeter they generally become. Near the end of
the season, the fruit can begin splitting apart on the tree.

$$photo(/img/foraging/sloes/washing.jpg)
    Washing fresh sloes ready for use.

The flesh of the sloes ranges from yellow-green to orange-red. It is usually
very tart then eaten raw. Mostly, I use them for
[/recipe/fruit-wine](making fruit wine), but they can also be used to flavour
gin and other spirits, or for making jams. The deep red colour they impart seems
to mostly come from the skins of the sloes.

