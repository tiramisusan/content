# Wild Hops

Young wild hop shoots are a delicious but much-overlooked food in the UK. In
Northern Italy, they are a prized delicacy of the spring, before they wither in
the summer sun. The milder British climate allows them to thrive for much more
of the year, but we all but ignore this abundant treat.

British people much more commonly use hop flowers, which are much larger on the
more domesticated varieties of the plant, to flavour beer and other beverages.
The shoots have a similar "hoppy" flavour, but with a sweeter flavour, a little
like asparagus.

## Habitat & Identification

Wild hops is a climbing vine that generally grows in proximity to water. It is
often accompanied by other vines of similar appearance, and so it is important
to avoid confusion.

The leaves of the wild hop have serrated edges, with three points, and these are
generally the most identifiable feature of the plant from a distance. Brambles
can sometimes have similar looking leaves, but all three points of the leaf of
the wild hop are connected, and it has no significant thorns.

The wild hop will often climb other plants, or fences. The stem spirals around
whatever it is climbing, and is adorned with pairs of leaves. The tips have a
distinctive appearence.

$$photo(/img/foraging/wild-hops/plant.jpg) Wild hop shoots in May

The most reliable way to identify a wild hop plant is to feel it. The wild hop
plant is covered in tiny rough hairs, that cause a rash in some people. Don't
worry though, they are destroyed during cooking. There are plenty of vines that
grow nearby to rivers that look like wild hops, but I have never seen one that
also feels like it to the touch.

## Cooking and Eating the Shoots

The best wild hop shoots to eat are the youngest. Try and go early in the
season, during April or May, though young shoots can be found for a lot of the
year. Pick the shoot a few inches below the tip, avoiding any that look thick or
woody. They are traditionally cooked into a frittata with egg, or into a
risotto, but can be used as a vegetable for any recipe. Simply fry them with a
little oil until they turn a deep green, and use however you want.

 * [*My recipe for wild hop risotto*](/recipe/wild-hop-risotto)
 * [*A very simple recipe for a wild hop frittata*](/recipe/wild-hop-frittata)

$$photo(/img/foraging/wild-hops/risotto.jpg)
    Delicious wild hop risotto, with wild garlic flowers.

