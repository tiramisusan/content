# Tiramisùsan

This is our website! Hello and Welcome!

## Contents

 * [*Recipe list*](recipe/) - A list of my recipes. These can also be found in
   LaTeX or PDF form in my [gitlab](https://gitlab.com/Xeom/recipes) repo. On
   this website, they are compiled from LaTeX to HTML.
 * [*Blog*](blog/) - A little blog for writing random things.
 * [*Moka Man*](md/moka-man.md) - A small friend.
 * [*Foraging*](md/foraging/) - A list of things I like to forage.
 * [*Pastebin*](/paste/) - A simple pastebin with support for
   uploading from Telegram.
 * [*TODO*](md/TODO.md) - A list of things I need to do.

## About

I've built this website with custom components, mostly for fun, but also as a
bit of a personal portfolio. In addition, I plan on hosting some personal
projects here, along with a blog, which should have some technical as well as
fun content.

 * This website is being hosted by a custom webserver called Servoiardi that
 I've written in Rust! The source code can be found on
 [gitlab](https://gitlab.com/tiramisusan/server).

 * The content of this website is contained in another
 [gitlab repository](https://gitlab.com/tiramisusan/content). The content of the
 website is arranged by matching URIs to services, for example static files or
 CGI in a
 [TOML config file](https://gitlab.com/tiramisusan/content/-/blob/main/README.md).

 * This page is compiled using a custom markdown-inspired markup language called
 [matthewdown](https://github.com/tiramisusan/matthewdown).
